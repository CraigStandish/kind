<?php

namespace Born\GeoLocation\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     * Creates a new table to store user country information on page visit
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $table = $installer->getConnection()
            ->newTable($installer->getTable('guest_location'))
            ->addColumn(
                'id',
                Table::TYPE_SMALLINT,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'Post ID'
            )
            ->addColumn('ip', Table::TYPE_TEXT, 100, ['nullable' => true, 'default' => null])
            ->addColumn('country', Table::TYPE_TEXT, 100, ['nullable' => true, 'default' => null])
            ->setComment('Guest Country');
        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }
}