<?php
namespace Born\GeoLocation\Model\ResourceModel\GuestLocation;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Define resource model and map id field
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Born\Geolocation\Model\GuestLocation', 'Born\GeoLocation\Model\ResourceModel\GuestLocation');
        $this->_map['fields']['id'] = 'main_table.id';
    }
}