<?php

namespace Born\GeoLocation\Model\ResourceModel;

/**
 * Created by PhpStorm.
 * User: stand
 * Date: 8/16/2017
 * Time: 2:07 PM
 */
/**
 * Class GuestLocation
 * @package Born\GeoLocation\Model\ResourceModel
 */
class GuestLocation extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @return void
     * Constructor to initialize parameters for entity Manager
     */
    protected function _construct()
    {
        $this->_init('guest_location', 'id');
    }
}