<?php

namespace Born\GeoLocation\Model;

/**
 * Created by PhpStorm.
 * User: stand
 * Date: 8/16/2017
 * Time: 2:03 PM
 */
/**
 * Class GuestLocation
 * @package Born\GeoLocation\Model
 */
class GuestLocation extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @var string
     */
    protected $ip;
    /**
     * @var string
     */
    protected $country;

    /**
     * Standard constructor for initializing the resourceModel
     */
    protected function _construct()
    {
        $this->_init('Born\GeoLocation\Model\ResourceModel\GuestLocation');
    }
}