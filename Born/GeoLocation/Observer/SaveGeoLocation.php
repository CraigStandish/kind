<?php

namespace Born\GeoLocation\Observer;

/**
 * Created by PhpStorm.
 * User: stand
 * Date: 8/16/2017
 * Time: 2:26 PM
 */
class SaveGeoLocation implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Born\GeoLocation\Model\GuestLocation
     */
    protected $guest;
    /**
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    protected $curl;
    /**
     * @var \Born\GeoLocation\Model\ResourceModel\GuestLocation
     */
    protected $guestcollection;

    /**
     * @var \Psr\Log\LoggerInterface $logger
     */
    protected $logger;

    /**
     * SaveGeoLocation constructor.  Sets a model to be used during execute and a curl connection.
     * @param \Born\GeoLocation\Model\GuestLocation $guest
     * @param \Born\GeoLocation\Model\ResourceModel\GuestLocation\Collection $guestcollection
     * @param \Magento\Framework\HTTP\Client\Curl $curl
     */
    public function __construct(\Born\GeoLocation\Model\GuestLocation $guest,
                                \Born\GeoLocation\Model\ResourceModel\GuestLocation $guestcollection,
                                \Magento\Framework\HTTP\Client\Curl $curl,
                                \Psr\Log\LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->guestcollection = $guestcollection;
        $this->curl = $curl;
        $this->guest = $guest;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     * Grabs the country of a user based on a curl call to an external service.  This is then saved into the model.
     * It will only be saved once in the table after a check for its existence fails.
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
         $this->curl->get("http://freegeoip.net/json/");
         $response = $this->curl->getBody();
         $data = json_decode($response, true);
         $res = $this->guestcollection->addFieldToFilter('ip', $data['ip'])->getAllIds();
         if (count($res) == 0) {
             $this->guest->setIp($data['ip']);
             $this->guest->setCountry($data['country_name']);
             try {
                 $this->guestcollection->save($this->guest);
             } catch (\Exception $e) {
                 $this->logger->error($e->getMessage());
             }
         }
        return $this;
    }
}