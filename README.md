### What is this repository for? ###

This is a small module that takes a users country from a call to http://freegeoip.net/json/ and records the visitors country into a table.  The observer checks if an entry already exists before saving the model.
